import json


EXCLUDE = 'exclude'
INCLUDE = 'include'
ACTIVE = 'active'
BOOL_INCLUDE = 1


class DSP:

    def __init__(self, object, name=None):
        self.name = name
        self.id = int(object['dsp_id'])
        self.ad_size_inclusion = object['filter_adsize'] == INCLUDE
        self.ad_sizes = set(json.loads(object['filter_adsize_list']).keys()) if object['filter_adsize_list'] != '' and object['filter_adsize'] is not None else set()
        self.app = object['filter_app'] == BOOL_INCLUDE
        self.banner = object['filter_banner'] == BOOL_INCLUDE
        self.country_inclusion = object['filter_country'] == INCLUDE
        self.countries = set(json.loads(object['filter_countrylist']).keys()) if object['filter_countrylist'] != '' else set()
        self.display = object['filter_display'] == BOOL_INCLUDE
        self.max_bidfloor = float(object['filter_max_bidfloor'])
        self.min_bidfloor = float(object['filter_min_bidfloor'])
        self.min_timeout = int(object['filter_min_timeout'])
        self.mobile = object['filter_mobile'] == BOOL_INCLUDE
        self.native = object['filter_native'] == BOOL_INCLUDE
        self.proximic = object['filter_proximic'] == BOOL_INCLUDE
        self.secure = object['filter_secure'] == BOOL_INCLUDE
        self.ssp_inclusion = object['filter_ssp'] == INCLUDE
        self.ssp_list = set(json.loads(object['filter_ssp_ids']).keys()) if object['filter_ssp_ids'] is not None and object['filter_ssp_ids'] != '' else set()
        self.traffic_qps = int(object['filter_traffic_qps'])
        self.traffic_qps_real = int(object['filter_traffic_qps_real'])
        self.traffic_qps_rpmlst = int(object['filter_traffic_qps_rpmlist'])
        self.video = object['filter_video'] == BOOL_INCLUDE
        self.video_linearity = object['filter_video_linearity'] == BOOL_INCLUDE
        self.video_size_inclusion = object['filter_videosize'] == INCLUDE
        self.video_sizes = set() if object['filter_videosize_list'] == '' or object['filter_videosize_list'] is None else set(json.loads(object['filter_videosize_list']))
        self.internal_bidder = object['is_internal_bidder'] == BOOL_INCLUDE
        self.settle_margin_percent = float(object['settle_margin_pct'])
        self.is_active = object['status'] == ACTIVE

    def __hash__(self):
        return hash(self.id)

    def __str__(self):
        return "dsp_{0}".format(self.id)

    def __unicode__(self):
        return self.__str__()

    def __repr__(self):
        return self.__str__()
