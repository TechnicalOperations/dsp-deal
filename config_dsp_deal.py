import json
import pandas
from util import sql
from dsp_deal.dsp import DSP
pandas.set_option('display.width', 5000)


with open('dsps.json', 'r') as f:
    dsps = json.loads(f.read())


regions = list(dsps[0]['regions'].keys())

try:
    deals = pandas.read_csv('deals.csv')
except FileNotFoundError:
    q = """
        SELECT external_deal_id AS deal_id,
            ssp_name,
            dsp_name,
            device_filter,
            imp_filter,
            media_filter,
            ad_size_filter,
            ad_size_list,
            video_size_filter,
            video_size_list,
            country_filter,
            country_list,
            in_view_percentage,
            segment_id,
            filter_type AS segment_filter
        FROM ods.rx.deal_view
        WHERE active = '1'
            AND CURRENT_TIMESTAMP() >= deal_start
            AND CURRENT_TIMESTAMP() <= deal_end
        GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15
    """
    con = sql.connect()
    deals = pandas.read_sql_query(q, con)
    sql.close()
    deals = deals.rename(columns={i: i.lower() for i in deals.columns})
    deals.to_csv('deals.csv', index=False)


deals['ssp_name'] = deals['ssp_name'].apply(json.loads)
deals['dsp_name'] = deals['dsp_name'].apply(json.loads)
deals['device_filter'] = deals['device_filter'].apply(json.loads)
deals['imp_filter'] = deals['imp_filter'].apply(json.loads)
deals['media_filter'] = deals['media_filter'].apply(json.loads)
deals['ad_size_filter'] = deals['ad_size_filter'].apply(lambda x: 'include' if x == 'include' else 'exclude')
deals['ad_size_list'] = deals['ad_size_list'].fillna('[]').apply(json.loads)
deals['video_size_filter'] = deals['video_size_filter'].apply(lambda x: 'include' if x == 'include' else 'exclude')
deals['video_size_list'] = deals['video_size_list'].fillna('[]').apply(json.loads)
deals['country_filter'] = deals['country_filter'].apply(lambda x: 'include' if x == 'include' else 'exclude')
deals['country_list'] = deals['country_list'].fillna('[]').apply(json.loads)
deals['segment_filter'] = deals['segment_filter'].apply(lambda x: 'include' if x == 'include' else 'exclude')
deals['segment_id'] = deals['segment_id'].fillna('[]').apply(json.loads)

deal_columns = ['deal_id', 'ssp_name', 'dsp_name', 'device_filter', 'imp_filter', 'media_filter', 'ad_size_filter',
           'ad_size_list', 'video_size_filter', 'video_size_list', 'country_filter', 'country_list',
           'in_view_percentage', 'segment_id', 'segment_filter'
]

all_dsps = list(set([z for i in list(deals['dsp_name']) for z in i]))


dsps = [dsp for dsp in dsps if dsp['name'] in all_dsps]
dsps = {
    region: {dsp['name']: DSP(dsp['regions'][region], name=dsp['name']) for dsp in dsps if region in dsp['regions']}
    for region in regions
}

def check_config(deal):
    output = {
        dsp: {
            region: {}
            for region in regions
        }
        for dsp in deal['dsp_name']
    }
    for region in regions:
        for dsp in deal['dsp_name']:
            dsp = dsps[region][dsp]
            if ('mobile' in deal['device_filter'] and not dsp.mobile) or ('display' in deal['device_filter'] and not dsp.display):
                output[dsp.name][region]['device'] = 'mobile' if 'mobile' in deal['device_filter'] and not dsp.mobile else 'display'
            if ('banner' in deal['imp_filter'] and not dsp.banner) or ('video' in deal['imp_filter'] and not dsp.video):
                output[dsp.name][region]['imp_type'] = 'banner' if 'banner' in deal['imp_filter'] and not dsp.banner else 'video'
            if 'app' in deal['media_filter'] and not dsp.app:
                output[dsp.name][region]['media_type'] = 'app'

            ads = set(deal['ad_size_list']) & dsp.ad_sizes
            if deal['ad_size_filter'] == 'include' and dsp.ad_size_inclusion and len(ads) == 0:
                output[dsp.name][region]['ad_size'] = ','.join(deal['ad_size_list'])
            if deal['ad_size_filter'] == 'include' and not dsp.ad_size_inclusion and len(ads) > 0:
                output[dsp.name][region]['ad_size'] = ','.join(ads)
            if deal['ad_size_filter'] == 'exclude' and dsp.ad_size_inclusion and len(ads) > 0:
                output[dsp.name][region]['ad_size'] = ','.join(ads)

            videos = set(deal['video_size_list']) & dsp.video_sizes
            if deal['video_size_filter'] == 'include' and dsp.video_size_inclusion and len(videos) == 0:
                output[dsp.name][region]['video_size'] = list(videos)
            if deal['video_size_filter'] == 'include' and not dsp.video_size_inclusion and len(videos) > 0:
                output[dsp.name][region]['video_size'] = list(videos)
            if deal['video_size_filter'] == 'exclude' and dsp.video_size_inclusion and len(videos) > 0:
                output[dsp.name][region]['video_size'] = list(videos)

            countries = set(deal['country_list']) & dsp.countries
            if deal['country_filter'] == 'include' and dsp.country_inclusion and len(countries) == 0:
                output[dsp.name][region]['country_code'] = ','.join(sorted(list(deal['country_list'])))
            if deal['country_filter'] == 'include' and not dsp.country_inclusion and len(countries) > 0:
                output[dsp.name][region]['country_code'] = ','.join(sorted(list(deal['country_list'])))
            if deal['country_filter'] == 'exclude' and dsp.country_inclusion and len(countries) > 0:
                output[dsp.name][region]['country_code'] = ','.join(sorted(list(dsp.countries)))
    return output


columns = pandas.Index(['deal_id', 'region', 'dsp', 'failures'])
deals['failure'] = deals.apply(check_config, axis=1)

deals = deals[['deal_id', 'failure']].to_dict(orient='records')
output = []
for deal in deals:
    for dsp_name, regions in deal['failure'].items():
        for region, items in regions.items():
            items = json.dumps(items)
            output.append({'deal_id': deal['deal_id'], 'dsp': dsp_name, 'region': region, 'failures': items})

deals = pandas.DataFrame(output)

# deals['fails'] = deals['failures'].apply(lambda x: 1 if len(x) > 2 else 0)
# no_requests = deals[['deal_id', 'dsp', 'fails']].groupby('deal_id').agg({'dsp': lambda x: len(x), 'fails': sum})
# no_requests = set(no_requests[(no_requests['dsp'] == no_requests['fails'])].index)
# del deals['fails']

# deals = deals[deals['deal_id'].isin(no_requests)]
deals = deals[deals['failures'].apply(lambda x: len(x) > 2)]

reasons = list(set([k for item in list(set(deals['failures'])) for k in json.loads(item)]))

q = """
    SELECT A.banner_w||'x'||A.banner_h AS ad_size,
        B.key::string AS dsp,
        A.media_type,
        A.imp_type,
        A.country_code,
        A.region_datacenter,
        B.value::string AS filter_reason,
        SUM(sample_rate) AS requests
    FROM rx.bidrequest AS A,
    LATERAL FLATTEN(input=>dsp_filter) B
    WHERE event_time_est >= DATE_TRUNC(DAY, DATEADD(DAY, -1, CURRENT_TIMESTAMP()))
        AND event_time_est < DATE_TRUNC(DAY, CURRENT_TIMESTAMP())
        AND B.key::string IN (
          'turn', 'appnexus', 'owneriq', 'owneriqtest', 'radium1webvid'
        )
    GROUP BY ad_size, dsp, A.media_type, A.imp_type, A.country_code, A.region_datacenter, filter_reason
"""
try:
    data = pandas.read_csv('requests.csv')
except FileNotFoundError:
    con = sql.connect()
    data = pandas.read_sql_query(q, con)
    sql.close()
    data = data.rename(columns={i: i.lower() for i in data.columns})
    data.to_csv('requests.csv', index=False)

print('\n'.join(set(data['filter_reason'])))
def sum_requests(row):
    tmp = data[(data['dsp'] == row['dsp']) & (data['region_datacenter'] == row['region'])]
    fails = json.loads(row['failures'])
    total = 0
    for key, value in fails.items():
        fr = None
        if key == 'media_type':
            fr = 'isAppFilter'
        if key == 'country_code':
            fr = 'countryInclude'
        if key == 'ad_size':
            fr = 'adsizeInclude'
        if key == 'imp_type':
            fr = 'bannerFilter'
        if ',' in value:
            value = value.split(',')
            total += tmp[tmp[key].isin(value) & (tmp['filter_reason'] == fr)]['requests'].sum()
        else:
            total += tmp[(tmp[key] == value) & (tmp['filter_reason'] == fr)]['requests'].sum()
    return total

deals['requests'] = deals.apply(sum_requests, axis=1)

print(deals[:10])
print(set(deals['dsp']))

print('')
for deal in set(deals['failures']):
    print(deal)

deals.to_excel('deal_dsp_failures.xlsx', index=False)

tmp = list(set(deals['dsp']))
for region, items in dsps.items():
    for t in tmp:
        print("{2} : {0} : {1}".format(t, items[t].country_inclusion, region))
